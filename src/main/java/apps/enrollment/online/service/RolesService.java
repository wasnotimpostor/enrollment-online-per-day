package apps.enrollment.online.service;

import apps.enrollment.online.entity.Roles;
import apps.enrollment.online.entity.enumerated.RoleName;

import java.util.Optional;

public interface RolesService {
    Optional<Roles> findByName(RoleName name);
    Object getAllRoles();
    Roles findById(Integer id) throws Exception;
}
