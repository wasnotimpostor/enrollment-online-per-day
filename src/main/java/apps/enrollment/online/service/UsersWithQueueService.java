package apps.enrollment.online.service;

import apps.enrollment.online.entity.UsersWithQueue;

public interface UsersWithQueueService {

    UsersWithQueue save(UsersWithQueue usersWithQueue);
}
