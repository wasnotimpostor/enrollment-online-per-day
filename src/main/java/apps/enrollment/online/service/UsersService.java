package apps.enrollment.online.service;

import apps.enrollment.online.dto.request.DtoLoginUser;
import apps.enrollment.online.dto.response.DtoUserById;
import apps.enrollment.online.entity.Users;
import org.springframework.security.core.userdetails.UserDetails;

public interface UsersService {
    Users save(Users users) throws Exception;
    DtoUserById findById(Integer id) throws Exception;
    Users delete(Integer id) throws Exception;
    Users getByEmail(String email);
    Boolean existsByEmail(String email);
    String login(DtoLoginUser request) throws Exception;
}
