package apps.enrollment.online.service;

import apps.enrollment.online.dto.request.DtoDateAndQuotas;
import apps.enrollment.online.entity.DateAndQuotas;

import java.time.LocalDate;

public interface DateAndQuotasService {

    DateAndQuotas save(DtoDateAndQuotas request);
    DateAndQuotas findById(Integer id) throws Exception;
    Boolean validateDate(LocalDate localDate);
    Integer countByDate(LocalDate localDate);
}
