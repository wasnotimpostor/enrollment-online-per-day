package apps.enrollment.online.service.serviceImpl;

import apps.enrollment.online.config.exception.CustomException;
import apps.enrollment.online.entity.Roles;
import apps.enrollment.online.entity.enumerated.RoleName;
import apps.enrollment.online.repository.RolesRepository;
import apps.enrollment.online.service.RolesService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class RolesServiceImpl implements RolesService {

    private final RolesRepository rolesRepository;

    private final JdbcTemplate jdbcTemplate;

    @Override
    public Optional<Roles> findByName(RoleName roleName) {
        return rolesRepository.findByName(roleName);
    }

    @Override
    public Object getAllRoles() {
        String q = "select * from roles where not id = 1";
        var list = jdbcTemplate.queryForList(q);
        return list;
    }

    @Override
    public Roles findById(Integer id) throws Exception{
        var role = rolesRepository.findById(id);
        if (!role.isPresent()) throw new CustomException("Role not found.", HttpStatus.NOT_FOUND);
        return role.get();
    }
}
