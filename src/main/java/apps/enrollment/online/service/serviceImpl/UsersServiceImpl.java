package apps.enrollment.online.service.serviceImpl;

import apps.enrollment.online.config.exception.CustomException;
import apps.enrollment.online.config.security.JwtProvider;
import apps.enrollment.online.dto.request.DtoLoginUser;
import apps.enrollment.online.dto.response.DtoUserById;
import apps.enrollment.online.entity.Users;
import apps.enrollment.online.repository.UsersRepository;
import apps.enrollment.online.service.RolesService;
import apps.enrollment.online.service.UsersService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;
    private final RolesService rolesService;
    private final JwtProvider jwtProvider;

    @Override
    public Users save(Users users) throws Exception {
        return usersRepository.save(users);
    }

    @Override
    public DtoUserById findById(Integer id) throws Exception {
        var user = usersRepository.findById(id);
        if (!user.isPresent()) throw new CustomException("User not found.", HttpStatus.NOT_FOUND);
        DtoUserById dtoUserById = DtoUserById.builder()
                .id(id)
                .email(user.get().getEmail())
                .first_name(user.get().getFirst_name())
                .last_name(user.get().getLast_name())
                .role(user.get().getRole())
                .build();
        return dtoUserById;
    }

    @Override
    public Users delete(Integer id) throws Exception {
        return null;
    }

    @Override
    public Users getByEmail(String email) {
        var user = usersRepository.findByEmail(email);
        return user.get();
    }

    @Override
    public Boolean existsByEmail(String email) {
        return usersRepository.existsByEmail(email);
    }

    @Override
    public String login(DtoLoginUser request) throws Exception {
        var user = this.getByEmail(request.getEmail());
        if (user == null){
            Users baru = Users.builder()
                    .first_name(request.getFirst_name())
                    .last_name(request.getLast_name())
                    .email(request.getEmail())
                    .oauth_id(request.getOauth_id())
                    .role(rolesService.findById(3))
                    .build();
            var save = usersRepository.save(baru);
        }
        if (user.getFirst_name().equals("") || user.getFirst_name() == null){
            user.setFirst_name(request.getFirst_name());
        }
        if (user.getLast_name().equals("") || user.getLast_name() == null){
            user.setLast_name(request.getLast_name());
        }
        var save = usersRepository.save(user);
        return this.generateToken(user);
    }

    public String generateToken(Users users){
        return jwtProvider.generateToken(users);
    }
}
