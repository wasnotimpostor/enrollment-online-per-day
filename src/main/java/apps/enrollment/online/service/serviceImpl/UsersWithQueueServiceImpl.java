package apps.enrollment.online.service.serviceImpl;

import apps.enrollment.online.entity.UsersWithQueue;
import apps.enrollment.online.repository.UsersWithQueueRepository;
import apps.enrollment.online.service.UsersWithQueueService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UsersWithQueueServiceImpl implements UsersWithQueueService {

    private final UsersWithQueueRepository repository;

    @Override
    public UsersWithQueue save(UsersWithQueue usersWithQueue) {
        return repository.save(usersWithQueue);
    }
}
