package apps.enrollment.online.service.serviceImpl;

import apps.enrollment.online.config.exception.CustomException;
import apps.enrollment.online.dto.request.DtoDateAndQuotas;
import apps.enrollment.online.entity.DateAndQuotas;
import apps.enrollment.online.entity.enumerated.Status;
import apps.enrollment.online.repository.DateAndQuotasRepository;
import apps.enrollment.online.service.DateAndQuotasService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.text.WordUtils;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@RequiredArgsConstructor
@Service
public class DateAndQuotasServiceImpl implements DateAndQuotasService {

    private final DateAndQuotasRepository repository;
    private final JdbcTemplate jdbcTemplate;

    public String generatePrefixCode(LocalDate localDate){
        var first = localDate.getDayOfWeek().toString().substring(0,3);
        var second = localDate.getDayOfMonth();
        var third = String.valueOf(localDate.getYear()).substring(2, 4);
        return first+second+third;
    }

    @Override
    public DateAndQuotas save(DtoDateAndQuotas request) {
        var prefix = this.generatePrefixCode(request.getDate());
        var quotas = 0;
        if (request.getQuotas() == null || request.getQuotas().equals(0) && request.getStatus().equals(Status.Open)){
            quotas = 100;
        }

        DateAndQuotas dateAndQuotas = DateAndQuotas.builder()
                .code(prefix)
                .date(request.getDate())
                .day(WordUtils.capitalizeFully(request.getDate().getDayOfWeek().name()))
                .quotas(quotas)
                .status(request.getStatus())
                .build();
        return repository.save(dateAndQuotas);
    }

    @Override
    public DateAndQuotas findById(Integer id) throws Exception {
        var exist = repository.findById(id);
        if (!exist.isPresent()) throw new CustomException("Data not found.", HttpStatus.NOT_FOUND);
        return exist.get();
    }

    @Override
    public Boolean validateDate(LocalDate localDate) {
        var exist = repository.validateDay(localDate);
        if (exist.isPresent()){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Integer countByDate(LocalDate localDate) {
        String q = "select * from date_and_quotas where date = "+localDate;
        var rsp = jdbcTemplate.queryForList(q);
        return rsp.size();
    }
}
