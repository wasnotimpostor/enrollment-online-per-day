package apps.enrollment.online.entity;

import apps.enrollment.online.entity.enumerated.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "users_with_queue")
public class UsersWithQueue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "user_id")
    private Integer user_id;

    @Column(name = "email")
    private String email;

    @Column(name = "date_code")
    private String date_code;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Column(name = "queue_number")
    private Integer queue_number;

    @JsonIgnore
    @CreationTimestamp
    @Column(name = "created_at")
    private Date created_at;

    @JsonIgnore
    @UpdateTimestamp
    @Column(name = "updated_at")
    private Date updated_at;
}
