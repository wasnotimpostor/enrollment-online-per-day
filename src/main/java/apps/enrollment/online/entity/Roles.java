package apps.enrollment.online.entity;

import apps.enrollment.online.entity.enumerated.RoleName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "roles")
public class Roles {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id", unique=true)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name="name")
    private RoleName name;
}
