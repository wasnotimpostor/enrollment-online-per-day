package apps.enrollment.online.entity.enumerated;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum Status {
    Open,
    Closed,
    Waiting,
    Done,
    Canceled;

    @JsonCreator
    public static Status value(String v) {
        try {
            return Status.valueOf(v.toUpperCase());
        } catch (Exception e) {
            return null;
        }
    }
}
