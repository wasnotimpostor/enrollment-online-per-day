package apps.enrollment.online.entity.enumerated;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum RoleName {
    Superadmin,
    Admin,
    User;

    @JsonCreator
    public static RoleName value(String v) {
        try {
            return RoleName.valueOf(v.toUpperCase());
        } catch (Exception e) {
            return null;
        }
    }
}
