package apps.enrollment.online.dto.request;

import apps.enrollment.online.entity.enumerated.Status;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
public class DtoDateAndQuotasUpdate {

    @NotNull(message = "Status is required.")
    @JsonProperty("status")
    private Status status;

    @NotNull(message = "Quotas is required.")
    @JsonProperty("quotas")
    private Integer quotas;
}
