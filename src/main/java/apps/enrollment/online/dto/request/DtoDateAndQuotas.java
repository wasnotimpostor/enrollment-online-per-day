package apps.enrollment.online.dto.request;

import apps.enrollment.online.entity.enumerated.Status;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DtoDateAndQuotas {

    @NotNull(message = "Date is required.")
    @JsonProperty("date")
    private LocalDate date;

    @JsonProperty("quotas")
    private Integer quotas;

    @JsonProperty("status")
    private Status status;
}
