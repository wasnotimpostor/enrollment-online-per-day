package apps.enrollment.online.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DtoRegisterUser {

    @NotNull(message = "Email is required")
    @JsonProperty("email")
    private String email;

    @NotNull(message = "Role id is required")
    @JsonProperty("roleId")
    private Integer roleId;
}
