package apps.enrollment.online.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class DtoUsersWithQueueReq {

    @JsonProperty("idDateAndQuotas")
    private Integer idDateAndQuotas;
}
