package apps.enrollment.online.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DtoLoginUser {

    @NotNull(message = "firstName is required")
    @JsonProperty("firstName")
    private String first_name;

    @JsonProperty("lastName")
    private String last_name;

    @NotNull(message = "email is required")
    @JsonProperty("email")
    private String email;

    @NotNull(message = "oauthId is required")
    @JsonProperty("oauthId")
    private String oauth_id;
}
