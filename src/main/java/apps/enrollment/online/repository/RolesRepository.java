package apps.enrollment.online.repository;

import apps.enrollment.online.entity.Roles;
import apps.enrollment.online.entity.enumerated.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RolesRepository extends JpaRepository<Roles, Integer> {
    Optional<Roles> findByName(RoleName name);
}
