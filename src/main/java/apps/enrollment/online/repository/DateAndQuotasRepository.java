package apps.enrollment.online.repository;

import apps.enrollment.online.entity.DateAndQuotas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.Optional;

public interface DateAndQuotasRepository extends JpaRepository<DateAndQuotas, Integer> {

    @Query(
            value = "select * from data_and_quotas where date = ?1",
            nativeQuery = true
    )
    Optional<DateAndQuotas> validateDay(LocalDate localDate);
}
