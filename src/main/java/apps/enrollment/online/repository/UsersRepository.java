package apps.enrollment.online.repository;

import apps.enrollment.online.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UsersRepository extends JpaRepository<Users, Integer> {
    Optional<Users> findByEmail(String email);

    Boolean existsByEmail(String email);

    @Query(
            value = "select * from users where email = binary(?1) and not id = ?2 ",
            nativeQuery = true
    )
    Optional<Users> validateEmail(String email, Integer id);
}
