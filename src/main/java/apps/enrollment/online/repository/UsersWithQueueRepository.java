package apps.enrollment.online.repository;

import apps.enrollment.online.entity.UsersWithQueue;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersWithQueueRepository extends JpaRepository<UsersWithQueue, Integer> {
}
