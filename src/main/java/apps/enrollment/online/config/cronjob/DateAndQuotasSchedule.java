package apps.enrollment.online.config.cronjob;

import apps.enrollment.online.dto.request.DtoDateAndQuotas;
import apps.enrollment.online.entity.enumerated.Status;
import apps.enrollment.online.service.DateAndQuotasService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDate;

@Configuration
@EnableScheduling
@RequiredArgsConstructor
public class DateAndQuotasSchedule {

    private final DateAndQuotasService dateAndQuotasService;

    @Scheduled(cron = "0 0 * * 1-5 ?")
    public void create(){
        LocalDate localDate = LocalDate.now();
        var exist = dateAndQuotasService.validateDate(localDate);
        if (exist.equals(false)){
            DtoDateAndQuotas request = DtoDateAndQuotas.builder()
                    .date(localDate)
                    .quotas(100)
                    .status(Status.Open)
                    .build();
            dateAndQuotasService.save(request);
        }
    }
}
