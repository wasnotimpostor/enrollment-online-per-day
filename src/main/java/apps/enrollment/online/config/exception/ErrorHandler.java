package apps.enrollment.online.config.exception;

import apps.enrollment.online.config.GlobalApiResponse;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.util.NoSuchElementException;

@RestControllerAdvice
@Slf4j
public class ErrorHandler {

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<?> handleNoHandlerFound(NoHandlerFoundException ce) {
        return GlobalApiResponse.builder()
                .timestamp(System.currentTimeMillis())
                .message(ce.getMessage())
                .status(HttpStatus.NOT_FOUND.value())
                .error(HttpStatus.NOT_FOUND.getReasonPhrase())
                .build()
                .entity();
    }

    @ExceptionHandler(CustomException.class)
    public ResponseEntity<?> handleCustomException(CustomException ce) {
        return GlobalApiResponse.builder()
                .timestamp(System.currentTimeMillis())
                .message(ce.getMessage())
                .status(ce.getHttpStatus().value())
                .error(ce.getHttpStatus().getReasonPhrase())
                .build()
                .entity();
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleException(Exception e) {
        return GlobalApiResponse.builder()
                .timestamp(System.currentTimeMillis())
                .message(e.getMessage())
                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .error(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
                .build()
                .entity();
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<?> handleAccessDeniedException() {
        return GlobalApiResponse.builder()
                .timestamp(System.currentTimeMillis())
                .message(HttpStatus.FORBIDDEN.getReasonPhrase())
                .status(HttpStatus.FORBIDDEN.value())
                .error(HttpStatus.FORBIDDEN.getReasonPhrase())
                .build()
                .entity();
    }

    @ExceptionHandler(ExpiredJwtException.class)
    public ResponseEntity<?> handleExpired(ExpiredJwtException e) {
        return GlobalApiResponse.builder()
                .timestamp(System.currentTimeMillis())
                .message("Token expired, please re-login")
                .status(HttpStatus.FORBIDDEN.value())
                .error(e.getMessage())
                .build()
                .entity();
    }

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<?> handleNoSuchElement(NoSuchElementException e) {
        return GlobalApiResponse.builder()
                .timestamp(System.currentTimeMillis())
                .message("Data not found.")
                .status(HttpStatus.NOT_FOUND.value())
                .error(HttpStatus.NOT_FOUND.getReasonPhrase())
                .build()
                .entity();
    }
}
