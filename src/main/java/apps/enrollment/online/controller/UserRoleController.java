package apps.enrollment.online.controller;

import apps.enrollment.online.config.GlobalApiResponse;
import apps.enrollment.online.service.RolesService;
import apps.enrollment.online.service.UsersService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
@RequiredArgsConstructor
@Slf4j
public class UserRoleController {

    private final UsersService usersService;
    private final RolesService rolesService;

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalApiResponse<?> userById(
            @PathVariable(name = "id") Integer user_id
    ) throws Exception {
        var rsp = usersService.findById(user_id);
        return new GlobalApiResponse<>(HttpStatus.OK.value(), rsp);
    }

    @GetMapping(value = "roles", produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalApiResponse<?> roles(){
        return new GlobalApiResponse<>(HttpStatus.OK.value(), rolesService.getAllRoles());
    }
}
