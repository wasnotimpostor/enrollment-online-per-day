package apps.enrollment.online.controller;

import apps.enrollment.online.config.GlobalApiResponse;
import apps.enrollment.online.config.exception.CustomException;
import apps.enrollment.online.dto.request.DtoLoginUser;
import apps.enrollment.online.dto.request.DtoRegisterUser;
import apps.enrollment.online.dto.response.DtoLoginRsp;
import apps.enrollment.online.entity.Users;
import apps.enrollment.online.service.RolesService;
import apps.enrollment.online.service.UsersService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/auth", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
@RequiredArgsConstructor
@Slf4j
public class AuthController {

    private final UsersService usersService;
    private final RolesService rolesService;

    @PreAuthorize("hasRole('Superadmin')"+
            "or hasRole('Admin')")
    @PostMapping(value = "register", produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalApiResponse<?> register(
            @RequestBody DtoRegisterUser request
    ) throws Exception {
        if (usersService.existsByEmail(request.getEmail())){
            throw new CustomException("Email sudah digunakan.", HttpStatus.CONFLICT);
        }

        Users users = Users.builder()
                .email(request.getEmail())
                .build();

        var role = rolesService.findById(request.getRoleId());
        users.setRole(role);

        var save = usersService.save(users);
        return new GlobalApiResponse<>(HttpStatus.OK.value(), save);
    }

    @PostMapping(value = "login", produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalApiResponse<?> login(
            @RequestBody DtoLoginUser request
    ) throws Exception {
        var rsp = usersService.login(request);
        DtoLoginRsp loginRsp = DtoLoginRsp.builder().token(rsp).build();
        return new GlobalApiResponse<>(HttpStatus.OK.value(), loginRsp);
    }
}
