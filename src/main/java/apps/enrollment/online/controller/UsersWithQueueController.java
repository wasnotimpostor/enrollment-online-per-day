package apps.enrollment.online.controller;

import apps.enrollment.online.config.GlobalApiResponse;
import apps.enrollment.online.config.exception.CustomException;
import apps.enrollment.online.config.security.JwtProvider;
import apps.enrollment.online.dto.request.DtoUsersWithQueueReq;
import apps.enrollment.online.entity.UsersWithQueue;
import apps.enrollment.online.entity.enumerated.Status;
import apps.enrollment.online.service.DateAndQuotasService;
import apps.enrollment.online.service.UsersService;
import apps.enrollment.online.service.UsersWithQueueService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;

@RestController
@RequestMapping(path = "/queue", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
@Slf4j
public class UsersWithQueueController {

    private final UsersService usersService;
    private final UsersWithQueueService usersWithQueueService;
    private final JwtProvider jwtProvider;
    private final DateAndQuotasService dateAndQuotasService;

    public UsersWithQueue build(HttpServletRequest request, Integer id_dateAndQuota) throws Exception {
        var token = request.getHeader("Authorization").replace("Bearer ", "");
        var email = jwtProvider.getEmailFromToken(token);
        var users = usersService.getByEmail(email);
        var dateAndQuotas = dateAndQuotasService.findById(id_dateAndQuota);
        UsersWithQueue usersWithQueue = UsersWithQueue.builder()
                .user_id(users.getId())
                .email(email)
                .date_code(dateAndQuotas.getCode())
                .status(Status.Waiting)
                .build();
        var checkQueue = dateAndQuotasService.countByDate(LocalDate.now());
        if (checkQueue == 100) {
            throw new CustomException("Maaf quota hari ini telah penuh, silahkan daftar kembali besok.", HttpStatus.BAD_REQUEST);
        } else {
            usersWithQueue.setQueue_number(checkQueue+1);
        }
        return usersWithQueue;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalApiResponse<?> create(
            @RequestBody DtoUsersWithQueueReq body,
            HttpServletRequest request
    ) throws Exception {
        var usersWithQueue = this.build(request, body.getIdDateAndQuotas());
        var save = usersWithQueueService.save(usersWithQueue);
        return new GlobalApiResponse<>(HttpStatus.OK.value(), save);
    }
}
