package apps.enrollment.online;

import org.apache.commons.text.WordUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;

@SpringBootTest(
		classes = OnlineApplication.class,
		webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "dev")
class OnlineApplicationTests {

	@Test
	void getDayOfWeek() {
		LocalDate localDate = LocalDate.now();
		System.out.println(WordUtils.capitalizeFully(localDate.getDayOfWeek().name()));
	}

	@Test
	void getDayOfMonth() {
		LocalDate localDate = LocalDate.now();
		System.out.println(localDate.getDayOfMonth());
	}

	@Test
	void getYear() {
		LocalDate localDate = LocalDate.now();
		System.out.println(localDate.getYear());
	}
}
